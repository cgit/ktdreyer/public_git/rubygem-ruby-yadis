# Generated from ruby-yadis-0.3.4.gem by gem2rpm -*- rpm-spec -*-
%global gem_name ruby-yadis
%if 0%{?el6}
%global rubyabi 1.8
%else
%global rubyabi 1.9.1
%endif

Summary: A library for performing Yadis service discovery
Name: rubygem-%{gem_name}
Version: 0.3.4
Release: 2%{?dist}
Group: Development/Languages
License: ASL 2.0
URL: http://www.openidenabled.com/yadis/libraries/ruby
Source0: http://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
BuildRequires: ruby(abi) = %{rubyabi}
%if 0%{?fedora}
BuildRequires: rubygems-devel
%else
BuildRequires: ruby(rubygems)
%endif
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?gem_extdir: %global gem_extdir %{_libdir}/gems/exts/%{gem_name}-%{version}}
%{!?ruby_sitelib: %global ruby_sitelib %(ruby -rrbconfig -e 'puts Config::CONFIG["sitelibdir"] ')}

%description
A library for performing Yadis service discovery


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
gem install --local --install-dir .%{gem_dir} \
            --force %{SOURCE0}

# Remove extra gemspec file
#rm -rf .%{gem_instdir}/%{gem_name}.gemspec

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


# The test suite makes a number of HTTP calls, which is unsuitable for
# Fedora's build system.
#%check
#pushd .%{gem_instdir}
# # The tests assume that cwd is the "test" directory.
# cd test
# #testrb2 -I../lib .
#popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README
%doc %{gem_instdir}/INSTALL
%doc %{gem_instdir}/COPYING
%{gem_instdir}/test
%{gem_instdir}/examples

%changelog
* Tue Feb 05 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.4-2
- RHEL 6 compatibility

* Fri Aug 03 2012 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.4-1
- Initial package, created by gem2rpm 0.8.1
